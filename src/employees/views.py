from django.db.models import Count

from django.shortcuts import render, redirect, get_object_or_404

from django.urls import reverse

from django.views import View
from django.views.generic.base import RedirectView
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    ListView,
    DeleteView,
    FormView
)

from users.models import SmsManager, RiskReport, Employee, Company, IncidentAccident
from .forms import EmployeeProfileFormModal, EmployeePicFormModal, EmployeeRiskReportForm, EmployeeIncidentAccidentForm

class EmployeeDashboardView(View):
    template_name = "employee/EmployeeDashboardStart.html"     

    def get(self, request, *args, **kwargs):
        # GET method
        context={}
        #Obtengo El empleado                
        id_ = self.request.user.id
        employee = get_object_or_404(Employee, user=id_)        
        #do something with employee
        return render(request, self.template_name, context)    
        
class EmployeeProfileView(DetailView):
    model           = Employee
    template_name   = 'employee/EmployeeDashboardProfile.html'    

    def get_object(self):        
        id_ = self.request.user.id
        return get_object_or_404(Employee, user=id_)

class EmployeeProfileUpdate(UpdateView):
    model = Employee
    template_name = 'employee/EmployeeProfileFormModal.html'
    form_class = EmployeeProfileFormModal

    def get_object(self):        
        id_ = self.request.user.id
        return get_object_or_404(Employee, user=id_)    

    def get_success_url(self):
            return reverse('employees:profile-employee')

class EmployeeProfilePicUpdateView(UpdateView):
    model           = Employee
    template_name   = 'employee/SmsMangerProfilePicFormModal.html'
    form_class      = EmployeePicFormModal

    def get_object(self):        
        id_ = self.request.user.id
        return get_object_or_404(Employee, user=id_)    

    def get_success_url(self):
        return reverse('employees:profile-employee')

#RISK REPORTS VIEWS
class EmployeeRiskReportListView(ListView):
    model = RiskReport
    #ordering = ('name', ) #para ordenar por fecha quiza tengamos que cambiar a Generic date views
    paginate_by = 10
    context_object_name = 'risk_reports'
    template_name = 'employee/Employee-RiskReportList.html'

    def get_context_data(self, **kwargs):
        #obtengo id del empleado
        id_ = self.request.user.id
        employee = get_object_or_404(Employee, user=id_)        
        kwargs['employee'] = employee
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        #obtengo id del empleado
        id_ = self.request.user.id
        employee = get_object_or_404(Employee, user=id_)
        #obtengo solo los reportes hechos por este empleado
        queryset = RiskReport.objects.filter(owner=employee.user)
        return queryset

class EmployeeRiskReportDetailView(DetailView):
    template_name = 'employee/Employee-RiskReportDetail.html'
    model         = RiskReport   

    def get_object(self):        
        id_ = self.kwargs.get("id")        
        return get_object_or_404(RiskReport, id=id_)

class EmployeeCreateRiskReportView(CreateView):
    model = RiskReport    
    template_name = 'employee/Employee-RiskReportCreate.html'
    form_class      = EmployeeRiskReportForm    

    def form_valid(self, form):        
        new_report = form.save(commit=False)
        new_report.owner = self.request.user
        #obtengo id del empleado
        id_ = self.request.user.id
        employee = get_object_or_404(Employee, user=id_)            
        new_report.of_company = employee.company        
        #guardo el reporte
        new_report.save()                
        return redirect('employees:risk-report-list')

###################################INCIDENTS-ACCIDENTS REPORTS VIEWS
class EmployeeIncidentAccidentListView(ListView):
    model = IncidentAccident    
    paginate_by = 10
    context_object_name = 'incidents_accidents'
    template_name = 'employee/Employee-IncidentAccidentList.html'

    def get_context_data(self, **kwargs):
        #obtengo id del empleado y el empleado asociado
        id_ = self.request.user.id
        employee = get_object_or_404(Employee, user=id_)        
        kwargs['employee'] = employee
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        #obtengo id del empleado
        id_ = self.request.user.id
        employee = get_object_or_404(Employee, user=id_)
        #obtengo solo los reportes hechos por este empleado
        queryset = IncidentAccident.objects.filter(owner=employee.user)
        return queryset

class EmployeeIncidentAccidentDetailView(DetailView):
    template_name = 'employee/Employee-IncidentAccidentDetail.html'
    model         = IncidentAccident   

    def get_object(self):        
        id_ = self.kwargs.get("id")        
        return get_object_or_404(IncidentAccident, id=id_)

class EmployeeCreateIncidentAccidentView(CreateView):
    model = IncidentAccident    
    template_name = 'employee/Employee-IncidentAccidentCreate.html'
    form_class      = EmployeeIncidentAccidentForm    

    def form_valid(self, form):        
        new_report = form.save(commit=False)
        new_report.owner = self.request.user
        #obtengo id del empleado
        id_ = self.request.user.id
        employee = get_object_or_404(Employee, user=id_)            
        new_report.of_company = employee.company        
        #guardo el reporte
        new_report.save()                
        return redirect('employees:incident-accident-list')