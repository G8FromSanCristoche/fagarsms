from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include, reverse_lazy

from .views import (    	
    AdminDashboardView,
    AdminSmsManagerListView,
    AdminDetailSmsManagerView,
    AdminCreateSmsManagerView,
    AdminUpdateSmsManagerView,
    AdminCompanyListView,
    AdminDetailCompanyView,
    AdminCreateCompanyView,
    AdminUpdateCompanyView,
    AdminEmployeeListView,
    AdminDetailEmployeeView,
    AdminCreateEmployeeView,
)

app_name = 'superuser'

urlpatterns = [    
    path('dashboard/', AdminDashboardView.as_view(), name='dashboard'),
    path('dashboard/sms-managers-list/', AdminSmsManagerListView.as_view(), name='sms-manager-list'),
    path('dashboard/sms-manager-detail/<int:id>/', AdminDetailSmsManagerView.as_view(), name='sms-manager-detail'),
    path('dashboard/sms-manager-create/', AdminCreateSmsManagerView.as_view(), name='sms-manager-create'),
    path('dashboard/sms-manager-update/<int:id>/', AdminUpdateSmsManagerView.as_view(), name='sms-manager-update'),
    path('dashboard/companys-list/', AdminCompanyListView.as_view(), name='company-list'),
    path('dashboard/comapny-detail/<int:id>/', AdminDetailCompanyView.as_view(), name='company-detail'),
    path('dashboard/company-create/', AdminCreateCompanyView.as_view(), name='company-create'),
    path('dashboard/company-update/<int:id>/', AdminUpdateCompanyView.as_view(), name='company-update'),
    path('dashboard/employees-list/', AdminEmployeeListView.as_view(), name='employee-list'),
    path('dashboard/employee-detail/<int:id>/', AdminDetailEmployeeView.as_view(), name='employee-detail'),
    path('dashboard/employee-create/', AdminCreateEmployeeView.as_view(), name='employee-create'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)