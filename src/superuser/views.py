from django.shortcuts import render, redirect, get_object_or_404

from django.views import View
from django.views.generic.base import RedirectView
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    ListView,
    DeleteView,
    FormView
)

from users.models import SmsManager, Company, Employee

from .forms import SignupSmsManagerForm,SignupEmployeeForm, CreateCompanyForm

class AdminDashboardView(View):
    template_name = "superuser/AdminDashboard.html"    

    def get(self, request, *args, **kwargs):
        # GET method
        context={}                
        return render(request, self.template_name, context)

#SMS MANAGERS VIEWS

class AdminSmsManagerListView(ListView):
    model = SmsManager
    ordering = ('name', )
    paginate_by = 10
    context_object_name = 'sms_managers'
    template_name = 'superuser/Admin-SmsManagerList.html'

    def get_queryset(self):
        queryset = SmsManager.objects.all()        
        return queryset

class AdminDetailSmsManagerView(DetailView):
    template_name = 'superuser/Admin-SmsManagerDetail.html'
    model           = SmsManager   

    def get_object(self):        
        id_ = self.kwargs.get("id")        
        return get_object_or_404(SmsManager, user=id_)

class AdminCreateSmsManagerView(CreateView):
    model = SmsManager
    form_class = SignupSmsManagerForm
    template_name = "superuser/Admin-SmsManagerCreate.html"

    def form_valid(self, form):            
        user = form.save(commit=True)            
        user.is_sms_manager = True
        #GuardoElUsuario
        user.save()    
        #CreoAlGerenteSMS
        sms_manager = SmsManager.objects.create(user=user)         
        return redirect('superuser:sms-manager-list')

class AdminUpdateSmsManagerView(UpdateView):
    model = SmsManager
    form_class = SignupSmsManagerForm
    template_name = "superuser/Admin-SmsManagerCreate.html"

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(SmsManager, user_id=id_)

    def form_valid(self, form):            
        user = form.save(commit=True)            
        user.is_sms_manager = True
        #GuardoElUsuario
        user.save()    
        #CreoAlGerenteSMS
        sms_manager = SmsManager.objects.create(user=user)         
        return redirect('superuser:sms-manager-list')

#COMPANYS VIEWS

class AdminCompanyListView(ListView):
    model = Company
    ordering = ('name')
    paginate_by = 10
    context_object_name = 'companys'
    template_name = 'superuser/Admin-CompanyList.html'

    def get_queryset(self):
        queryset = Company.objects.all()        
        return queryset

class AdminDetailCompanyView(DetailView):
    template_name = 'superuser/Admin-CompanyDetail.html'
    model         = Company   

    def get_object(self):        
        id_ = self.kwargs.get("id")        
        return get_object_or_404(Company, id=id_)

class AdminCreateCompanyView(CreateView):
    model = Company
    form_class= CreateCompanyForm    
    template_name = "superuser/Admin-CompanyCreate.html"
  
    def get_context_data(self, **kwargs):
        kwargs['sms_managers'] = SmsManager.objects.all()
        return super().get_context_data(**kwargs)       

    def form_valid(self, form):        
        new_company = form.save()        
        new_company.save()
        return redirect('superuser:company-list')

class AdminUpdateCompanyView(UpdateView):
    model = Company
    form_class= CreateCompanyForm    
    template_name = "superuser/Admin-CompanyCreate.html"

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Company, id=id_)

    def get_context_data(self, **kwargs):
        kwargs['sms_managers'] = SmsManager.objects.all()
        return super().get_context_data(**kwargs)       

    def form_valid(self, form):        
        new_company = form.save()        
        new_company.save()
        return redirect('superuser:company-list')

#EMPLOYEE VIEWS

class AdminEmployeeListView(ListView):
    model = Employee
    ordering = ('name')
    paginate_by = 10
    context_object_name = 'employees'
    template_name = 'superuser/Admin-EmployeeList.html'

    def get_queryset(self):
        queryset = Employee.objects.all()        
        return queryset

class AdminDetailEmployeeView(DetailView):
    template_name = 'superuser/Admin-EmployeeDetail.html'
    model           = Employee   

    def get_object(self):        
        id_ = self.kwargs.get("id")        
        return get_object_or_404(Employee, user=id_)

class AdminCreateEmployeeView(CreateView):
    model = Employee
    form_class= SignupEmployeeForm    
    template_name = "superuser/Admin-EmployeeCreate.html"
  
    def form_valid(self, form):
        print('form valid')
        #Guardo el usuario        
        user = form.save(commit=True)
        user.is_employee = True
        user.save()
        #obtengo la compañia seleccionada
        selected_company = form.cleaned_data.get('companys') #aqui obtengo el objeto company y puedo comprabarlo con print(selected_company.id) o print(selected_company)
        #Creo el empleado
        #employee = Employee.objects.create(user=user)
        employee = Employee.objects.create(user=user, company=selected_company)
        #Asigno el empleado a la empresa seleccinada (MANYTOMANYFIELD)        
        #employee.company.add(selected_company)                
        employee.save()
        return redirect('superuser:employee-list')