from django.db import models
from django.db.models.signals import post_save

from django.contrib.auth.models import AbstractUser, AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin

from django.utils.translation import ugettext_lazy as _

from django.dispatch import receiver


#USERMANAGER esto para usar el correo electronico como identificador de usuario
class MyUserManager(BaseUserManager):
    """
    A custom user manager to deal with emails as unique identifiers for auth
    instead of usernames. The default that's used is "UserManager"
    """
    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The Email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)        
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(email, password, **extra_fields)

#Clase basica para tener varios tipos de usuarios
class User(AbstractBaseUser, PermissionsMixin):
    email           = models.EmailField(unique=True, blank=False)
    #flags que determinan el rol del usuario
    is_sms_manager	= models.BooleanField(default=False)
    is_employee	= models.BooleanField(default=False)
    #flag staff
    is_superuser = models.BooleanField(
        _('superuser status'),
        default=False,
        help_text=_('Do you think you can be a Superuser?'),
    )
    #flag staff
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this site.'),
    )
    #falg activo
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )    
    
    USERNAME_FIELD = 'email'

    objects = MyUserManager()

    def __str__(self):
        return self.email

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

#Clase para los gerentes SMS
class SmsManager(models.Model):
    user        = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    name        = models.CharField(max_length=50)
    last_name   = models.CharField(max_length=50)    
    #ci          = models.IntegerField(unique=True)
    career      = models.CharField(max_length=50, default='')     
    phone       = models.IntegerField(null=False, default=0)
    photo       = models.ImageField(upload_to='sms_managers/profile_pics/', default='sms_managers/profile_pics/default_user_profile_pic.png')    

TYPE_OF_COMPANY = (
    ('1', 'Airport'),#Aeropuerto
    ('2', 'Aeroline'),#Personas Juridicas
    ('3', 'ciac'),#centro de instrucción
    ('4', 'omac'),#Taller OMAC    
)

class Company(models.Model):
    #todo compañia debe tener un gerente sms
    #tambien muchas compañias pueden tener un mismo gerete sms
    company_sms_manager = models.ForeignKey(SmsManager, on_delete=models.CASCADE, related_name='managed_by')
    name                = models.CharField(max_length=100)
    type_of_company     = models.CharField(max_length=1, choices=TYPE_OF_COMPANY, blank=True)
    #rav_type           = models.CharField(max_length=1, choices=TYPE_OF_RAP, blank=True)
    #una compañia puede tener mas de rav
    #hay regulaciones por otros paises rap (peru), far(estados unidos), etc
    #rav_number         = models.IntegerField(null=False, default=0)
    phone               = models.IntegerField(null=False, default=0)
    company_logo        = models.ImageField(upload_to='companys/company_logos/', default='companys/company_logos/default_company_logo.png')
    rif                 = models.IntegerField(null=False, default=0)
    company_mail        = models.EmailField(blank=False, default='mail@mail.com')

class Employee(models.Model):
    user        = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    #muchos empleados pertenecen a una compañia
    company     = models.ForeignKey(Company, on_delete=models.CASCADE, related_name = 'employee_of_company', default = False)
    name        = models.CharField(max_length=50)
    last_name   = models.CharField(max_length=50)    
    phone       = models.IntegerField(null=False, default=0)
    photo       = models.ImageField(upload_to='employees/profile_pics/', default='employess/profile_pics/default_user_profile_pic.png')    

TYPE_OF_RISK = (
    ('1', 'Relacionado con vuelo'),
    ('2', 'E.P.P. (Equipos de protección personal)'),
    ('3', 'Procedimientos'),
    ('4', 'Mantenimiento Aeronáutico'),
    ('5', 'FOD (Objetos extraños)'),
    ('6', 'Orden y limpieza'),
    ('7', 'Acto inseguro'),
    ('8', 'Otros'),
)

class RiskReport(models.Model):
    owner           = models.ForeignKey(User, on_delete=models.CASCADE, related_name='reports_of_owner')
    of_company      = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='reports_of_company')
    date            = models.DateField()
    place           = models.CharField(max_length=100)
    type_of_risk    = models.CharField(max_length=1, choices=TYPE_OF_RISK, blank=True)
    photo1          = models.ImageField(upload_to='risk_reports/', default='risk_reports/risk.png')
    photo2          = models.ImageField(upload_to='risk_reports/', default='risk_reports/risk.png')
    photo3          = models.ImageField(upload_to='risk_reports/', default='risk_reports/risk.png')
    photo4          = models.ImageField(upload_to='risk_reports/', default='risk_reports/risk.png')
    photo5          = models.ImageField(upload_to='risk_reports/', default='risk_reports/risk.png')
    description     = models.TextField(blank=True)
    recmendations   = models.TextField(blank=True, default='')

class IncidentAccident(models.Model):
    owner           = models.ForeignKey(User, on_delete=models.CASCADE, related_name='incident_accident_of_owner')
    of_company      = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='incident_accident_of_company')
    date            = models.DateField()
    place           = models.CharField(max_length=100)    
    photo1          = models.ImageField(upload_to='incidents_accidents/', default='incidents_accidents/incident_accident.jpg')
    photo2          = models.ImageField(upload_to='incidents_accidents/', default='incidents_accidents/incident_accident.jpg')
    photo3          = models.ImageField(upload_to='incidents_accidents/', default='incidents_accidents/incident_accident.jpg')
    photo4          = models.ImageField(upload_to='incidents_accidents/', default='incidents_accidents/incident_accident.jpg')
    photo5          = models.ImageField(upload_to='incidents_accidents/', default='incidents_accidents/incident_accident.jpg')
    description     = models.TextField(blank=True)

