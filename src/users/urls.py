from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.auth.views import(
    PasswordResetView,
    PasswordResetDoneView,
    PasswordResetConfirmView,
    PasswordResetCompleteView,    
    )

from django.conf import settings
from django.conf.urls.static import static

from django.urls import path, include, reverse_lazy

from .views import (
    #SignupView,
	#SignupLinkSentView,
	#SignupActivationView,
	LoginView,
    LogoutView,    
    #CustomPasswordResetCompleteView,
)

app_name = 'users'

urlpatterns = [
    #URLS FOR SIGNUP PROCESS
    #path('signup/', SignupView.as_view(), name='signup'),
    #path('signup/linksent/', SignupLinkSentView.as_view(), name='signup-linksent'),
    #path('activation/<uid64>/<token>/', SignupActivationView.as_view(), name='signup-activation'),
    #URLS FOR LOGIN & LOGOUT
    path('login/', LoginView.as_view(), name='login'),       
    path('logout/', LogoutView.as_view(), name='logout'),
    #URLS FOR PASSWORD RESET
    path('password-reset/', PasswordResetView.as_view(
        template_name='users/password_reset_form.html',
        email_template_name='users/password_reset_email.html',
        subject_template_name='users/password_reset_subject.txt',
        success_url=reverse_lazy('users:password_reset_done')
        ), name='password-reset'),
    path('password-reset-done/',PasswordResetDoneView.as_view(
        template_name='users/password_reset_done.html'
        ), name='password_reset_done'),
    path('password-reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(
        template_name='users/password_reset_confirm.html',
        success_url=reverse_lazy('users:password_reset_complete')
        ), name='password_reset_confirm'),
     #path('password_reset_complete/', CustomPasswordResetCompleteView.as_view(), name='password_reset_complete'),    
]