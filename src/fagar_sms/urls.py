from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url

urlpatterns = [
    path('admin/', admin.site.urls),    
    path('',include('users.urls')),
    path('',include('superuser.urls')),
    path('',include('sms_manager.urls')),
    path('',include('sms_manager.urls_many_companys')),
    path('',include('sms_manager.urls_mini_views')),
    path('',include('employees.urls')),
    path('',include('landing.urls')),
    url(r'api/', include('api.urls')),   
]
