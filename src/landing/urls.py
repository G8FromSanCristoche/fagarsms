from django.urls import path
from .views import (
    LandingView,
    #AboutView,
    #ContactView,
    #FAQView,
    #PricingView,
)

app_name = 'landing'

urlpatterns = [
    path('', LandingView.as_view(), name='landing'),
    #path('about/', AboutView.as_view(), name='about'),
    #path('contact/', ContactView.as_view(), name='contact'),
    #path('faq/', FAQView.as_view(), name='faq'),
    #path('pricing/', PricingView.as_view(), name='pricing'),
]