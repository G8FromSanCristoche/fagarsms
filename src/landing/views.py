from django.shortcuts import render, get_object_or_404, redirect

from django.views import View
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    ListView,
    DeleteView,
    FormView
)

import json

class LandingView(View):
    template_name = "landing/LandingBase.html"

    def get(self, request, *args, **kwargs):
        # GET method
        context={}                
        return render(request, self.template_name, context)