from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include, reverse_lazy

from .views_mini import (    	
    NotificationsMiniView,
    ManagingMiniView,      
)

app_name = 'sms_manager_mini_views'

urlpatterns = [    
    path('sms-manager/mini/notifications/', NotificationsMiniView.as_view(), name='notifications'),
    path('sms-manager/mini/managing/', ManagingMiniView.as_view(), name='managing'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)