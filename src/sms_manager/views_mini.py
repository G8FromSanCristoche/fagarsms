from django.db.models import Count

from django.shortcuts import render, redirect, get_object_or_404

from django.urls import reverse

from django.views import View
from django.views.generic.base import RedirectView
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    ListView,
    DeleteView,
    FormView
)

from users.models import SmsManager, Employee, User, Company

class NotificationsMiniView(View):
    #model = Notification    
    #context_object_name = 'notifications'
    template_name = 'sms_manager/mini_views/NotificationsMiniView.html'

    def get(self, request, *args, **kwargs):
        context={}
        manager = get_object_or_404(SmsManager, user=self.request.user.id)
        context["manager"] = manager        
        return render(request, self.template_name, context)

class ManagingMiniView(View):
    model = Company    
    context_object_name = 'companys_mini'
    template_name = 'sms_manager/mini_views/ManagingMiniView.html'

    def get(self, request, *args, **kwargs):
        context={}
        manager = get_object_or_404(SmsManager, user=self.request.user.id)
        number = manager.managed_by.count()
        if number == 1:
            context["mini_Manage_many"] = False
            company = manager.managed_by.first()
            context["mini_company"] = company
        if number > 1:
            context["mini_Manage_many"]  = True
            companys = Company.objects.filter(company_sms_manager=manager)
            context["mini_companys"] = companys
        return render(request, self.template_name, context)