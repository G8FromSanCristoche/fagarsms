from django.db.models import Count

from django.shortcuts import render, redirect, get_object_or_404

from django.urls import reverse

from django.views import View
from django.views.generic.base import RedirectView
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    ListView,
    DeleteView,
    FormView
)

from users.models import SmsManager, RiskReport, Company, Employee, IncidentAccident, User
from .forms import SmsManagerProfileFormModal, SmsManagerProfilePicFormModal, SmsManagerRiskReportForm, SignupEmployeeForm, SmsManagerIncidentAccidentForm

class SmsManagerDashboardView(View):    
    template_name = "sms_manager/SmsManagerDashboardStart.html"        

    def get(self, request, *args, **kwargs):
        # GET method
        context={}
        #Obtengo El manager                
        id_ = self.request.user.id
        manager = get_object_or_404(SmsManager, user=id_)
        #obtengo cuantas compañias maneja el manager
        number = manager.managed_by.count() #"managed_by" es el related name definido en el modelo de compañia
        #si el manager no maneja ningun prestador de servicios
        if number == 0:        
            context["Manage_cero"] = True
        #si el manager gestiona un solo prestador de servicios
        if number == 1:        
            context["Manage_many"] = False            
            company = get_object_or_404(Company, company_sms_manager=manager)
            context["company"] = company
        if number > 1:        
            context["Manage_many"] = True
            context["select_a_company"] = False
            companys = Company.objects.filter(company_sms_manager=manager)
            context["companys"] = companys
        return render(request, self.template_name, context)

class SmsManagerDashboardCompanySelectedView(View):    
    template_name = "sms_manager/SmsManagerDashboardStart.html"        

    def get(self, request, *args, **kwargs):        
        context={}        
        id_ = self.request.user.id
        manager = get_object_or_404(SmsManager, user=id_)                
        context["Manage_many"] = True        
        context["select_a_company"] = True
        company = get_object_or_404(Company, id=self.kwargs.get("id_company"))
        context["company"] = company
        companys = Company.objects.filter(company_sms_manager=manager)
        context["companys"] = companys
        return render(request, self.template_name, context)

class SmsManagerProfileView(DetailView):
    model           = SmsManager
    template_name   = 'sms_manager/SmsManagerDashboardProfile.html'

    def get_context_data(self, **kwargs):        
        id_ = self.request.user.id
        manager = get_object_or_404(SmsManager, user=id_)        
        number = manager.managed_by.count() #"managed_by" es el related name definido en el modelo de compañia        
        if number == 1:
            kwargs["Manage_many"]       = False
            company                     = manager.managed_by.first() #Como las compañias solo pueden tener un gerente sms, nos basta con el primero de la relación
            kwargs['sms_managers']      = manager            
        if number > 1:
            kwargs["Manage_many"]       = True
            kwargs["select_a_company"]  = True
            #compañia que seleccione
            company                     = get_object_or_404(Company, id=self.kwargs.get("id_company"))
            kwargs["company"]           = company
            #todas las compañias
            companys                    = Company.objects.filter(company_sms_manager=manager)
            kwargs["companys"]          = companys
            kwargs['sms_managers']      = manager
            kwargs['employees']         = Employee.objects.filter(company=company)

        return super().get_context_data(**kwargs)

    def get_object(self):        
        id_ = self.request.user.id
        return get_object_or_404(SmsManager, user=id_)

class SmsManagerProfileUpdate(UpdateView):
    model = SmsManager
    template_name = 'sms_manager/SmsMangerProfileFormModal.html'
    form_class = SmsManagerProfileFormModal

    def get_object(self):        
        id_ = self.request.user.id
        return get_object_or_404(SmsManager, user=id_)    

    def get_success_url(self):
            return reverse('sms_manager:profile-sms-manager')

class SmsManagerProfilePicUpdateView(UpdateView):
    model           = SmsManager
    template_name   = 'sms_manager/SmsMangerProfilePicFormModal.html'
    form_class      = SmsManagerProfilePicFormModal    

    def get_object(self):        
        id_ = self.request.user.id
        return get_object_or_404(SmsManager, user=id_)    

    def get_success_url(self):
        return reverse('sms_manager:profile-sms-manager')


#RISK REPORTS VIEWS
class SmsManagerRiskReportListView(ListView):
    model = RiskReport
    #ordering = ('name', ) #para ordenar por fecha quiza tengamos que cambiar a Generic date views
    paginate_by = 10
    context_object_name = 'risk_reports'
    template_name = 'sms_manager/SmsManager-RiskReportList.html'

    def get_context_data(self, **kwargs):
        #obtengo id del manager
        id_ = self.request.user.id
        manager = get_object_or_404(SmsManager, user=id_)
        #obtengo cuantas compañias maneja el manager
        number = manager.managed_by.count() #"managed_by" es el related name definido en el modelo de compañia        
        if number == 1:
            kwargs["Manage_many"] = False
            company                 = manager.managed_by.first() #Como las compañias solo pueden tener un gerente sms, nos basta con el primero de la relación
            kwargs['sms_managers']  = manager            
        if number > 1:
            kwargs["Manage_many"]       = True
            kwargs["select_a_company"]  = True
            #compañia que seleccione
            company                     = get_object_or_404(Company, id=self.kwargs.get("id_company"))
            kwargs["company"]           = company
            #todas las compañias
            companys                    = Company.objects.filter(company_sms_manager=manager)
            kwargs["companys"]          = companys
            kwargs['sms_managers']      = manager
            kwargs['employees']         = Employee.objects.filter(company=company)

        return super().get_context_data(**kwargs)

    def get_queryset(self):
        #obtengo id del manager
        id_ = self.request.user.id
        manager = get_object_or_404(SmsManager, user=id_)
        #obtengo cuantas compañias maneja el manager
        number = manager.managed_by.count() #"managed_by" es el related name definido en el modelo de compañia        
        if number == 1:
            company = manager.managed_by.first() #Como las compañias solo pueden tener un gerente sms, nos basta con el primero de la relación
            queryset = RiskReport.objects.filter(of_company=company)
        if number > 1:
            context={}
            context["Manage_one"] = True
            context["select_a_company"] = True
            company = get_object_or_404(Company, id=self.kwargs.get("id_company"))            
            queryset = RiskReport.objects.filter(of_company=company)
        return queryset

class SmsManagerRiskReportDetailView(DetailView):
    template_name = 'sms_manager/SmsManager-RiskReportDetail.html'
    model         = RiskReport

    def get_context_data(self, **kwargs):
        #obtengo el reporte
        id_ = self.kwargs.get("id")        
        report = get_object_or_404(RiskReport, id=id_)
        owner = User.objects.get(email = report.owner)
        print(owner.id)
        if owner.is_employee:
            kwargs['reporter']      = get_object_or_404(Employee, user_id= owner.id)
            kwargs['employee']      = True
            kwargs['sms_manager']   = False
        if owner.is_sms_manager:
            kwargs['reporter']      = get_object_or_404(SmsManager, user_id= owner.id)
            kwargs['employee']      = False
            kwargs['sms_manager']   = True
        
        return super().get_context_data(**kwargs)   

    def get_object(self):        
        id_ = self.kwargs.get("id")        
        return get_object_or_404(RiskReport, id=id_)

class SmsManagerCreateRiskReportView(CreateView):
    model = RiskReport    
    template_name = 'sms_manager/SmsManager-RiskReportCreate.html'
    form_class      = SmsManagerRiskReportForm
    context_object_name = 'risk_reports'

    def form_valid(self, form):        
        new_report = form.save(commit=False)
        new_report.owner = self.request.user
        #obtengo id del manager
        id_ = self.request.user.id
        manager = get_object_or_404(SmsManager, user=id_)
        #obtengo cuantas compañias maneja el manager
        number = manager.managed_by.count() #"managed_by" es el related name definido en el modelo de compañia
        if number == 0:        
            print('aun no gestiono nada')
        if number == 1:
            company = manager.managed_by.first() #Como las compañias solo pueden tener un gerente sms, nos basta con el primero de la relación            
            new_report.of_company = company
        if number > 1:        
            print('este manager gestiona varias empresas deberia seleccionar primero una empresa antes de crear un reporte')
        #despues de la logica anterior se guarda el reporte
        new_report.save()                
        return redirect('sms_manager:risk-report-list')

###################################INCIDENTS-ACCIDENTS-VIEWS
class SmsManagerIncidentAccidentListView(ListView):
    model = IncidentAccident
    #ordering = ('name', ) #para ordenar por fecha quiza tengamos que cambiar a Generic date views
    paginate_by = 10
    context_object_name = 'incidents_accidents'
    template_name = 'sms_manager/SmsManager-IncidentAccidentList.html'

    def get_context_data(self, **kwargs):
        #obtengo id del manager
        id_ = self.request.user.id
        manager = get_object_or_404(SmsManager, user=id_)
        #obtengo cuantas compañias maneja el manager
        number = manager.managed_by.count() #"managed_by" es el related name definido en el modelo de compañia        
        if number == 1:
            kwargs["Manage_many"] = False
            company                 = manager.managed_by.first() #Como las compañias solo pueden tener un gerente sms, nos basta con el primero de la relación
            kwargs['sms_managers']  = manager
            kwargs['employees']     = Employee.objects.filter(company=company)            
        if number > 1:
            kwargs["Manage_many"]       = True        
            kwargs["select_a_company"]  = True
            #compañia que seleccione
            company                     = get_object_or_404(Company, id=self.kwargs.get("id_company"))
            kwargs["company"]           = company
            #todas las compañias
            companys                    = Company.objects.filter(company_sms_manager=manager)
            kwargs["companys"]          = companys
            kwargs['sms_managers']      = manager
            kwargs['employees']         = Employee.objects.filter(company=company)
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        #obtengo id del manager
        id_ = self.request.user.id
        manager = get_object_or_404(SmsManager, user=id_)
        #obtengo cuantas compañias maneja el manager
        number = manager.managed_by.count() #"managed_by" es el related name definido en el modelo de compañia        
        if number == 1:
            company = manager.managed_by.first() #Como las compañias solo pueden tener un gerente sms, nos basta con el primero de la relación
            queryset = IncidentAccident.objects.filter(of_company=company)
        if number > 1:
            context={}
            context["Manage_one"] = True
            context["select_a_company"] = True
            company = get_object_or_404(Company, id=self.kwargs.get("id_company"))            
            queryset = IncidentAccident.objects.filter(of_company=company)
        return queryset

class SmsManagerIncidentAccidentDetailView(DetailView):
    template_name = 'sms_manager/SmsManager-IncidentAccidentDetail.html'
    model         = IncidentAccident

    def get_context_data(self, **kwargs):
        #obtengo el reporte
        id_ = self.kwargs.get("id")        
        report = get_object_or_404(RiskReport, id=id_)
        owner = User.objects.get(email = report.owner)
        print(owner.id)
        if owner.is_employee:
            kwargs['reporter']      = get_object_or_404(Employee, user_id= owner.id)
            kwargs['employee']      = True
            kwargs['sms_manager']   = False
        if owner.is_sms_manager:
            kwargs['reporter']      = get_object_or_404(SmsManager, user_id= owner.id)
            kwargs['employee']      = False
            kwargs['sms_manager']   = True
        
        return super().get_context_data(**kwargs)   

    def get_object(self):        
        id_ = self.kwargs.get("id")        
        return get_object_or_404(IncidentAccident, id=id_)

class SmsManagerCreateIncidentAccidentView(CreateView):
    model = IncidentAccident    
    template_name = 'sms_manager/SmsManager-IncidentAccidentCreate.html'
    form_class      = SmsManagerIncidentAccidentForm
    
    def form_valid(self, form):        
        new_report = form.save(commit=False)
        new_report.owner = self.request.user
        #obtengo id del manager
        id_ = self.request.user.id
        manager = get_object_or_404(SmsManager, user=id_)
        #obtengo cuantas compañias maneja el manager
        number = manager.managed_by.count() #"managed_by" es el related name definido en el modelo de compañia
        if number == 0:        
            print('aun no gestiono nada')
        if number == 1:
            company = manager.managed_by.first() #Como las compañias solo pueden tener un gerente sms, nos basta con el primero de la relación            
            new_report.of_company = company
        if number > 1:        
            print('este manager gestiona varias empresas deberia seleccionar primero una empresa antes de crear un reporte')
        #despues de la logica anterior se guarda el reporte
        new_report.save()                
        return redirect('sms_manager:incident-accident-list')

###################################################EMPLOYEE VIEWS
class SmsManagerEmployeeListView(ListView):
    model = Employee
    ordering = ('name')
    paginate_by = 10
    context_object_name = 'employees'
    template_name = 'sms_manager/SmsManager-EmployeeList.html'

    def get_context_data(self, **kwargs):
        #obtengo id del manager
        id_ = self.request.user.id
        manager = get_object_or_404(SmsManager, user=id_)
        #obtengo cuantas compañias maneja el manager
        number = manager.managed_by.count() #"managed_by" es el related name definido en el modelo de compañia        
        if number == 1:
            kwargs["Manage_many"] = False
            company                 = manager.managed_by.first() #Como las compañias solo pueden tener un gerente sms, nos basta con el primero de la relación
            kwargs['sms_managers']  = manager            
        if number > 1:
            kwargs["Manage_many"]       = True
            kwargs["select_a_company"]  = True
            #compañia que seleccione
            company                     = get_object_or_404(Company, id=self.kwargs.get("id_company"))
            kwargs["company"]           = company
            #todas las compañias
            companys                    = Company.objects.filter(company_sms_manager=manager)
            kwargs["companys"]          = companys
            kwargs['sms_managers']      = manager
            kwargs['employees']         = Employee.objects.filter(company=company)

        return super().get_context_data(**kwargs)

    def get_queryset(self):
        #obtengo id del manager
        id_ = self.request.user.id
        manager = get_object_or_404(SmsManager, user=id_)
        #obtengo cuantas compañias maneja el manager
        number = manager.managed_by.count()
        if number == 0:        
            print('aun no gestiono nada')
        if number == 1:
            #obtengo la unica compañia que se esta gestionado
            company = manager.managed_by.first()
            #selecciono solo a los empleados que pertenecen a esa compañia
            queryset = Employee.objects.filter(company=company)  
        if number > 1:        
            context={}            
            company = get_object_or_404(Company, id=self.kwargs.get("id_company"))            
            queryset = RiskReport.objects.filter(of_company=company)
        return queryset


class SmsManagerDetailEmployeeView(DetailView):
    template_name = 'sms_manager/SmsManager-EmployeeDetail.html'
    model           = Employee   

    def get_object(self):        
        id_ = self.kwargs.get("id")        
        return get_object_or_404(Employee, user=id_)

class SmsManagerCreateEmployeeView(CreateView):
    model = Employee
    form_class= SignupEmployeeForm    
    template_name = "sms_manager/SmsManager-EmployeeCreate.html"
  
    def form_valid(self, form):
        print('form valid')
        #Guardo el usuario        
        user = form.save(commit=True)
        user.is_employee = True
        user.save()
        #obtengo el manager
        id_ = self.request.user.id
        manager = get_object_or_404(SmsManager, user=id_)
        #obtengo cuantas compañias maneja el manager
        number = manager.managed_by.count()
        if number == 0:        
            print('aun no gestiono nada')
        if number == 1:
            #obtengo la unica compañia que se esta gestionado
            company = manager.managed_by.first()
            #creo el empleado dentro de esa compañia
            employee = Employee.objects.create(user=user, company=company)    
        if number > 1:        
            print('este manager gestiona varias empresas deberia seleccionar primero una empresa antes de crear un reporte')        
        employee.save()
        return redirect('sms_manager:employee-list')
