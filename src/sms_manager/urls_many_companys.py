from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include, reverse_lazy

from .views import (    	
    SmsManagerDashboardView,
    SmsManagerDashboardCompanySelectedView,
    SmsManagerProfileView,
    SmsManagerProfileUpdate,
    SmsManagerProfilePicUpdateView,
    SmsManagerRiskReportListView,
    SmsManagerRiskReportDetailView,    
    SmsManagerCreateRiskReportView,
    SmsManagerIncidentAccidentListView,
    SmsManagerIncidentAccidentDetailView,
    SmsManagerCreateIncidentAccidentView,
    SmsManagerEmployeeListView,
    SmsManagerDetailEmployeeView,
    SmsManagerCreateEmployeeView,    
)

app_name = 'sms_manager_many_companys'

urlpatterns = [    
    path('sms-manager/dashboard/company/<int:id_company>/', SmsManagerDashboardCompanySelectedView.as_view(), name='dashboard'),
    path('sms-manager/dashboard/company/<int:id_company>/profile/', SmsManagerProfileView.as_view(), name='profile-sms-manager'),
    path('sms-manager/dashboard/company/<int:id_company>/risk-report-list/', SmsManagerRiskReportListView.as_view(), name='risk-report-list'),
    path('sms-manager/dashboard/company/<int:id_company>/incident-accident-list/', SmsManagerIncidentAccidentListView.as_view(), name='incident-accident-list'),
    path('sms-manager/dashboard/<int:id_company>/employees-list/', SmsManagerEmployeeListView.as_view(), name='employee-list'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)