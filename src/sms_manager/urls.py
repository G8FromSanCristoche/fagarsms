from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include, reverse_lazy

from .views import (    	
    SmsManagerDashboardView,
    SmsManagerProfileView,
    SmsManagerProfileUpdate,
    SmsManagerProfilePicUpdateView,
    SmsManagerRiskReportListView,
    SmsManagerRiskReportDetailView,    
    SmsManagerCreateRiskReportView,
    SmsManagerIncidentAccidentListView,
    SmsManagerIncidentAccidentDetailView,
    SmsManagerCreateIncidentAccidentView,
    SmsManagerEmployeeListView,
    SmsManagerDetailEmployeeView,
    SmsManagerCreateEmployeeView,    
)

app_name = 'sms_manager'

urlpatterns = [    
    path('sms-manager/dashboard/', SmsManagerDashboardView.as_view(), name='dashboard'),
    path('sms-manager/dashboard/profile/', SmsManagerProfileView.as_view(), name='profile-sms-manager'),
    path('sms-manager/dashboard/profile_update/', SmsManagerProfileUpdate.as_view(), name='update-profile-sms-manager'),
    path('sms-manager/dashboard/profile_pic_update/', SmsManagerProfilePicUpdateView.as_view(), name='update-profile-pic-sms-manager'),    
    path('sms-manager/dashboard/risk-report-list/', SmsManagerRiskReportListView.as_view(), name='risk-report-list'),
    path('sms-manager/dashboard/risk-report-detial/<int:id>/', SmsManagerRiskReportDetailView.as_view(), name='risk-report-detail'),
    path('sms-manager/dashboard/risk-report-create/', SmsManagerCreateRiskReportView.as_view(), name='risk-report-create'),
    path('sms-manager/dashboard/incident-accident-list/', SmsManagerIncidentAccidentListView.as_view(), name='incident-accident-list'),
    path('sms-manager/dashboard/incident-accident-detial/<int:id>/', SmsManagerIncidentAccidentDetailView.as_view(), name='incident-accident-detail'),
    path('sms-manager/dashboard/incident-accident-create/', SmsManagerCreateIncidentAccidentView.as_view(), name='incident-accident-create'),
    path('sms-manager/dashboard/employees-list/', SmsManagerEmployeeListView.as_view(), name='employee-list'),
    path('sms-manager/dashboard/employee-detail/<int:id>/', SmsManagerDetailEmployeeView.as_view(), name='employee-detail'),
    path('sms-manager/dashboard/employee-create/', SmsManagerCreateEmployeeView.as_view(), name='employee-create'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)