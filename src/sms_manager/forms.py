from django import forms
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from users.models import User, SmsManager, RiskReport, Company, IncidentAccident

class SmsManagerProfileFormModal(forms.ModelForm):

    class Meta:
        model = SmsManager
        fields = ['name', 'last_name', 'phone']

class SmsManagerProfilePicFormModal(forms.ModelForm):

    class Meta:
        model = SmsManager
        fields = ['photo',]

#CREAR REPORTE DE RIESGO
class SmsManagerRiskReportForm(forms.ModelForm):

    class Meta:
        model = RiskReport
        fields = ['date', 'place', 'type_of_risk', 'photo1', 'photo2', 'photo3', 'photo4', 'photo5', 'description', 'recmendations',]  
        """widgets = {
          'description': forms.Textarea(attrs={'rows':20, 'cols':15}),
        }"""

    #esto sirve para añadir class a un field del form
    def __init__(self, *args, **kwargs):
        super(SmsManagerRiskReportForm, self).__init__(*args, **kwargs)
        self.fields['description'].widget.attrs\
            .update({                
                'class': 'materialize-textarea'
            })
        self.fields['recmendations'].widget.attrs\
            .update({                
                'class': 'materialize-textarea'
            })

#CREAR INCIDENTE / ACCIDENTE
class SmsManagerIncidentAccidentForm(forms.ModelForm):

    class Meta:
        model = IncidentAccident
        fields = ['date', 'place', 'photo1', 'photo2', 'photo3', 'photo4', 'photo5', 'description',]  

    #esto sirve para añadir class a un field del form
    def __init__(self, *args, **kwargs):
        super(SmsManagerIncidentAccidentForm, self).__init__(*args, **kwargs)
        self.fields['description'].widget.attrs\
            .update({                
                'class': 'materialize-textarea'
            })

#CREAR EMPLEADO
class SignupEmployeeForm(UserCreationForm):	
	email 		= forms.EmailField(max_length=300, help_text='Required')
	password1 	= forms.CharField(label="",max_length=50,min_length=8, widget=forms.PasswordInput())
	password2 	= forms.CharField(label="",max_length=50,min_length=8, widget=forms.PasswordInput())

	class Meta(UserCreationForm.Meta):
		model = User
		fields = ('email', 'password1', 'password2',)

    #Chequeamos que el mail no exista (ya registrado)
	def clean_email(self):
		# Get the email
		email = self.cleaned_data.get('email')

		# Check to see if any users already exist with this email as a username.
		try:
			match = User.objects.get(email=email)
		except User.DoesNotExist:
			# Unable to find a user, this is fine
			return email

		# A user was found with this as a username, raise an error.
		raise forms.ValidationError('Esta dirección de correo ya esta en uso')

	#Override clean method to check 
	def clean(self):
		password1 = self.cleaned_data.get('password1')
		password2 = self.cleaned_data.get('password2')

		"""if password1 and password1 != password2:			
			#raise forms.ValidationError("Las dos contraseñas deben coincidir")
			#raise forms.ValidationError({'password1': ["Passwords must be the same."]})"""

		return self.cleaned_data